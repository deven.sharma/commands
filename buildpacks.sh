function runBuildpack()
{
    echo "======================================== Running Buildpacks ================================================================\n"
    docker run  -v /var/run/docker.sock:/var/run/docker.sock  -v $PWD:/workspace -w /workspace buildpacksio/pack build my_image --builder  heroku/buildpacks:18
}
runBuildpack
