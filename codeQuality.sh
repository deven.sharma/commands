function checkForCodeQuality() {
    echo "======================================== Running Code Quality Container ================================================================\n"

    docker run  --env SOURCE_CODE=$PWD --volume $PWD:/code  --volume /var/run/docker.sock:/var/run/docker.sock registry.gitlab.com/gitlab-org/ci-cd/codequality:${VERSION:-latest} /code
}

checkForCodeQuality